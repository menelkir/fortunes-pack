Fortunes Pack
=============

This is a huge fortune pack grabbed from many places.
Feel free to create a PR if you want to add more to this pack, make sure
you list the source so I can add it here.

Mind that there's several licenses, so every repository will probably have
a different license (if you care about it).

=======
Sources
=======

BOFH: http://www.cs.wisc.edu/~ballard/bofh/

Calvin & Hobbes: http://www.netmeister.org/misc.html

Chuck Norris facts: http://k-lug.org~kessler/projects.html

Confucius: https://billy.wolfe.casa/fortunes/confucius

Doctor Who - Classic Series: https://tvtropes.org/pmwiki/pmwiki.php/Quotes/DoctorWhoClassicSeriesDoctors

Doctor Who - New Series: https://tvtropes.org/pmwiki/pmwiki.php/Quotes/DoctorWhoNewSeriesDoctors

Firefly: https://en.wikiquote.org/wiki/Firefly_(TV_series)

Futurama: http://www.netmeister.org/misc.html

Hitchhiker's Guide to Galaxy: https://www.splitbrain.org/projects/fortunes/hg2g

Limericks: https://billy.wolfe.casa/fortunes/limericks

Matrix: https://en.wikiquote.org/wiki/The_Matrix_%28franchise%29

Metal Gear: http://en.wikiquote.org/wiki/Metal_Gear

Monty Python: http://www.sacred-texts.com/neu/mphg/mphg.htm

Portal: https://github.com/outadoc/portal-fortunes

Protolol: http://attrition.org/misc/ee/protolol.txt

South Park: http://www.splitbrain.org/projects/fortunes/starwars

Vim Tips: https://github.com/hobbestigrou/vimtips-fortune

Wikiquote Fortune Collection: https://github.com/maandree/wikiquote-fortune-collection/

X-Files: http://www.splitbrain.org/projects/fortunes/xfiles
